﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    
    public float initialVelocity = 100;
    Rigidbody2D rb;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // vystreleni balonku
        if(Input.GetButtonDown("Fire1"))
        {
            transform.parent = null;

            rb.AddForce(new Vector2(initialVelocity, initialVelocity));

        }

        // rozdeleni balonku
        if(Input.GetKeyDown(KeyCode.S))
        {
            Vector3 posun = new Vector3(-0.5f, 0.0f, 0.0f);

            GameObject novaKulicka = Instantiate(this.gameObject);

            novaKulicka.transform.position = transform.position + posun;

            Vector2 smerNoveKulicky = new Vector2(-rb.velocity.x, rb.velocity.y);

            print("aktualni smer kulicky: " + smerNoveKulicky);

            novaKulicka.GetComponent<Rigidbody2D>().AddForce(50.0f*smerNoveKulicky);
        
        }
    }
}
