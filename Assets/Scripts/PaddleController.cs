﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{
    public float speed;

    void Test()
    {

    }
        
    void Update()
    {

        if (Input.GetKey(KeyCode.RightArrow))
        {


            float posX = transform.position.x;
            posX = posX + speed;
            posX = Mathf.Clamp(posX, -2.6f, 2.6f);
            transform.position = new Vector3(posX, transform.position.y, transform.position.z);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            float posX = transform.position.x;
            posX = posX - speed;
            posX = Mathf.Clamp(posX, -2.6f, 2.6f);
            transform.position = new Vector3(posX, transform.position.y, transform.position.z);
        }

        
        

    }
}

